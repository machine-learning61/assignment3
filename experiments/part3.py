import numpy as np
import pandas as pd

from sklearn.decomposition import PCA, FastICA
from sklearn.random_projection import SparseRandomProjection
from sklearn.ensemble import RandomForestClassifier

from experiments import part1

NUM_COMPONENTS = {
    'pca': {'bank': 3, 'wine': 2},
    'ica': {'bank': 2, 'wine': 4},
    'rca': {'bank': 16, 'wine': 9},
    'rf': {'bank': 3, 'wine': 4}
}

def print_and_write(string):
    print(string)
    f = open('part3.txt', "a")
    f.write(string + '\n')
    f.close()

def part3(data):
    # Initial Print and Write
    print('---Part 3---')
    f = open('part3.txt', "w")
    f.write('\n---Part3---\n')
    f.close()

    dimension_reduction_algs = [
        {'instance': PCA, 'name': 'pca'}, 
        {'instance': FastICA, 'name': 'ica'}, 
        {'instance': SparseRandomProjection, 'name': 'rca'}, 
        {'instance': RandomForestClassifier, 'name': 'rf'}, 
    ]

    reduced_data_all = []
    for alg in dimension_reduction_algs:
        for dataset in data:
            label = '{}-{}'.format(alg['name'], dataset['label'])
            reduced_data = {'label': label, 'X': None, 'y_true': dataset['y_true']}
            if alg['name'] == 'rf':
                features = alg['instance']().fit(dataset['X'], dataset['y_true']).feature_importances_
                num_features = NUM_COMPONENTS[alg['name']][dataset['label']]
                reduced_features_arr = np.argpartition(features, -num_features)[-num_features:]
                reduced_data['X'] = dataset['X'].iloc[:, reduced_features_arr]
            else:
                n_components = NUM_COMPONENTS[alg['name']][dataset['label']]
                reduced_data['X'] = alg['instance'](n_components=n_components).fit_transform(dataset['X'])
            
            part1([reduced_data], 'part3')