
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json

from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.metrics import homogeneity_score, silhouette_score, rand_score

from seaborn import pairplot, color_palette

# GLOBALS
KMEANS_NUM_CLUSTERS = {
    # first two are for part1
    'bank': 3, 
    'wine': 2,
    # Rest are for part3
    'pca-bank': 5,
    'pca-wine': 8,
    'ica-bank': 7,
    'ica-wine': 7,
    'rca-bank': 4,
    'rca-wine': 7,
    'rf-bank': 19, #9
    'rf-wine': 7,
    }
EM_NUM_CLUSTERS = {
    'bank': 9,
    'wine': 5,
    # Rest are for part3
    'pca-bank': 5, #12... maybe rerun
    'pca-wine': 5,
    'ica-bank': 4,
    'ica-wine': 9,
    'rca-bank': 11, #19
    'rca-wine': 6,
    'rf-bank': 4, #10
    'rf-wine': 4,
    }

def generate_silhouette_elbow_plots_both(data):
    cluster_range = np.arange(2, 21)
    kmeans_silhouette = {'bank': [], 'wine': []}
    em_bic = {'bank': [], 'wine': []}

    kmeans_homo = {'bank': [], 'wine': []}
    em_homo = {'bank': [], 'wine': []}

    kmeans_rand = {'bank': [], 'wine': []}
    em_rand = {'bank': [], 'wine': []}

    for dataset in data:
        for i in cluster_range:
            print(i)
            # KMEANS
            kmeans = KMeans(n_clusters=i).fit(dataset['X'])
            labels = kmeans.labels_

            score = silhouette_score(dataset['X'], labels)
            homog = homogeneity_score(dataset['y_true'], labels)
            rand = rand_score(dataset['y_true'], labels)

            kmeans_silhouette[dataset['label']].append(score)
            kmeans_homo[dataset['label']].append(homog)
            kmeans_rand[dataset['label']].append(rand)

            # EM
            em = GaussianMixture(n_components=i).fit(dataset['X'])
            score = em.bic(dataset['X'])
            labels = em.predict(dataset['X'])

            homog = homogeneity_score(dataset['y_true'], labels)
            rand = rand_score(dataset['y_true'], labels)

            em_bic[dataset['label']].append(score)
            em_homo[dataset['label']].append(homog)
            em_rand[dataset['label']].append(rand)
    
    plot_title = "Clustering Silhoutte Scores - KMeans"
    plt.figure(plot_title)
    plt.plot(cluster_range, kmeans_silhouette['wine'])
    plt.plot(cluster_range, kmeans_silhouette['bank'])
    plt.legend(['Wine data', 'Bank data'])
    plt.xticks(np.arange(2, 21, 2))
    plt.title(plot_title)
    plt.xlabel('Number of Clusters')
    plt.ylabel('Silhouette Score')
    plt.savefig('plots/part1/sihouette-kmeans-both')	
    plt.close(plot_title)

    plot_title = "Clustering BIC Scores - EM"
    plt.figure(plot_title)
    plt.plot(cluster_range, em_bic['wine'] / np.mean(em_bic['wine']))
    plt.plot(cluster_range, em_bic['bank'] / np.mean(em_bic['bank']) / 6)
    plt.xticks(np.arange(2, 21, 2))
    plt.title(plot_title)
    plt.legend(['Wine data', 'Bank data (scaled)'])
    plt.xlabel('Number of Clusters')
    plt.ylabel('BIC (normalized and scaled)')
    plt.savefig('plots/part1/bic-em-both')	
    plt.close(plot_title)

    plot_title = "Clustering Validation Scores - KMeans"
    plt.figure(plot_title)
    plt.plot(cluster_range, kmeans_homo['wine'], linestyle='dashed', color='blue')
    plt.plot(cluster_range, kmeans_homo['bank'], linestyle='dashed', color='red')
    plt.plot(cluster_range, kmeans_rand['wine'], color='blue')
    plt.plot(cluster_range, kmeans_rand['bank'], color='red')
    plt.xticks(np.arange(2, 21, 2))
    plt.title(plot_title)
    plt.legend(['Wine data - Homogeneity', 'Bank data - Homogeneity', 'Wine data - Rand', 'Bank data - Rand'])
    plt.xlabel('Number of Clusters')
    plt.ylabel('Validation Scores')
    plt.savefig('plots/part1/validation-kmeans-both')	
    plt.close(plot_title)

    plot_title = "Clustering Validation Scores - EM"
    plt.figure(plot_title)
    plt.plot(cluster_range, em_homo['wine'], linestyle='dashed', color='blue')
    plt.plot(cluster_range, em_homo['bank'], linestyle='dashed', color='red')
    plt.plot(cluster_range, em_rand['wine'], color='blue')
    plt.plot(cluster_range, em_rand['bank'], color='red')
    plt.xticks(np.arange(2, 21, 2))
    plt.title(plot_title)
    plt.legend(['Wine data - Homogeneity', 'Bank data - Homogeneity', 'Wine data - Rand', 'Bank data - Rand'])
    plt.xlabel('Number of Clusters')
    plt.ylabel('Validation Scores')
    plt.savefig('plots/part1/validation-em-both')	
    plt.close(plot_title)
            



def generate_silhouette_elbow_plots(data, filepath='part1'):
    cluster_range = np.arange(2, 21)
    for dataset in data:
        # Get best numnber of clusters
        kmeans_scores = []
        em_scores = []
        homogenuitys_kmeans = []
        rands_kmeans = []

        homogenuitys_em = []
        rands_em = []

        for i in cluster_range:
            kmeans = KMeans(n_clusters=i).fit(dataset['X'])
            labels = kmeans.labels_

            score = silhouette_score(dataset['X'], labels)
            homog = homogeneity_score(dataset['y_true'], labels)
            rand = rand_score(dataset['y_true'], labels)

            homogenuitys_kmeans.append(homog)
            rands_kmeans.append(rand)


            kmeans_scores.append(score)

            em = GaussianMixture(n_components=i).fit(dataset['X'])
            score = em.bic(dataset['X'])
            em_scores.append(score)

            labels = em.predict(dataset['X'])
            homog = homogeneity_score(dataset['y_true'], labels)
            rand = rand_score(dataset['y_true'], labels)

            homogenuitys_em.append(homog)
            rands_em.append(rand)

        
        plot_title = "Clustering Silhoutte Score - Kmeans - {}".format(dataset['label'])
        plt.figure(plot_title)
        plt.plot(cluster_range, kmeans_scores)
        plt.xticks(np.arange(2, 21, 2))
        plt.title(plot_title)
        plt.xlabel('Number of Clusters')
        plt.ylabel('Silhouette Score')
        plt.savefig('plots/{}/kmeans-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)

        plot_title = "Clustering BIC Score - EM - {}".format(dataset['label'])
        plt.figure(plot_title)
        plt.plot(cluster_range, em_scores)
        plt.xticks(np.arange(2, 21, 2))
        plt.title(plot_title)
        plt.xlabel('Number of Clusters')
        plt.ylabel('BIC')
        plt.savefig('plots/{}/em-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)

        plot_title = "Clustering Validation - Kmeans  - {}".format(dataset['label'])
        plt.figure(plot_title)
        plt.plot(cluster_range, homogenuitys_kmeans)
        plt.plot(cluster_range, rands_kmeans)
        plt.legend(['Homogenuity', 'Rand Score'])
        plt.xticks(np.arange(2, 21, 2))
        plt.title(plot_title)
        plt.xlabel('Number of Clusters')
        plt.savefig('plots/{}/kmeans-validation-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)

        plot_title = "Clustering Validation - EM  - {}".format(dataset['label'])
        plt.figure(plot_title)
        plt.plot(cluster_range, homogenuitys_em)
        plt.plot(cluster_range, rands_em)
        plt.legend(['Homogenuity', 'Rand Score'])
        plt.xticks(np.arange(2, 21, 2))
        plt.title(plot_title)
        plt.xlabel('Number of Clusters')
        plt.savefig('plots/{}/em-validation-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)

# Custom Cluster analysis
def analyze_clusters(y_pred, y_true, num_clusters, filepath):
    clusters = np.empty(num_clusters, dtype=object)
    clusters[...] = [[] for _ in range(clusters.shape[0])]

    for i in range(len(y_pred)):
        cluster_num = y_pred[i]
        clusters[cluster_num].append(y_true[i])
    
    for cluster in clusters:
        unique, counts = np.unique(cluster, return_counts=True)
        print_and_write('len: ' + str(len(cluster)) + ' ' + str(dict(zip(unique, counts))), filepath)

def print_and_write(string, filepath='part1'):
    print(string)
    f = open('{}.txt'.format(filepath), "a")
    f.write(string + '\n')
    f.close()

def part1(data, filepath='part1'):
    # Initial write to whipe file
    print('----Part 1----')
    

    option = "w" if filepath == 'part1' else "a"
    f = open('{}.txt'.format(filepath), option)
    f.write("Part 1 Results\n")
    f.close()

    # This is expensive, so dont run often
    if filepath == 'part1':
        generate_silhouette_elbow_plots_both(data)

    for dataset in data:
        # Kmeans
        kmeans = KMeans(n_clusters=KMEANS_NUM_CLUSTERS[dataset['label']]).fit(dataset['X'])
        y_pred = kmeans.labels_
        homog = homogeneity_score(dataset['y_true'], y_pred)
        rand = rand_score(dataset['y_true'], y_pred)

        if filepath == 'part1':
            # Do some analysis
            analyze_clusters(y_pred, dataset['y_true'], KMEANS_NUM_CLUSTERS[dataset['label']], filepath)

            # Pairplots
            new_df = dataset['X'].copy(deep=True)
            new_df['labels'] = y_pred

            # Pairplot all
            pairplot(new_df, x_vars=new_df.columns, hue='labels')
            plt.savefig('plots/{}/scatter-kmeans-{}-all'.format(filepath, dataset['label']))	
            plt.close()

            x_vars_wine = ['density', 'total sulfur dioxide']
            y_vars_wine = ['pH']

            x_vars_bank = ['duration', 'campaign', 'cons.price.idx']
            y_vars_bank = ['age']

            if dataset['label'] == 'wine':
                g = pairplot(new_df, x_vars=x_vars_wine, y_vars=y_vars_wine, hue='labels')
                g.fig.suptitle('Wine - Kmeans Pairplot')
                plt.savefig('plots/{}/wine-density-pH-kmeans'.format(filepath, dataset['label']))	
            else:
                g = pairplot(new_df, x_vars=x_vars_bank, y_vars=y_vars_bank, hue='labels')
                g.fig.suptitle('Bank - Kmeans Pairplot')
                plt.savefig('plots/{}/bank-density-pH-kmeans'.format(filepath, dataset['label']))	
            plt.close()
        
        if filepath == 'part3':
            # Pairplot all
            new_df = pd.DataFrame(dataset['X'])
            new_df['labels'] = y_pred
            g = pairplot(new_df, hue='labels')
            plt.savefig('plots/{}/scatter-kmeans-{}-all'.format(filepath, dataset['label']))	
            plt.close()

        print_and_write("{} | Kmeans homogenuity: {}".format(dataset['label'], homog), filepath)
        print_and_write("{} | Kmeans rand_score: {}".format(dataset['label'], rand), filepath)
        
        # EM
        y_pred = GaussianMixture(n_components=EM_NUM_CLUSTERS[dataset['label']]).fit_predict(dataset['X'])
        homog = homogeneity_score(dataset['y_true'], y_pred)
        rand = rand_score(dataset['y_true'], y_pred)

        if filepath == 'part1':
            analyze_clusters(y_pred, dataset['y_true'], EM_NUM_CLUSTERS[dataset['label']], filepath)
            # Pairplots
            new_df = dataset['X'].copy(deep=True)
            new_df['labels'] = y_pred

            # Pairplot all
            g = pairplot(new_df, x_vars=new_df.columns, hue='labels')
            plt.savefig('plots/{}/scatter-em-{}-all'.format(filepath, dataset['label']))	
            plt.close()

            if dataset['label'] == 'wine':
                g = pairplot(new_df, x_vars=x_vars_wine, y_vars=y_vars_wine, hue='labels')
                g.fig.suptitle('Wine - EM Pairplot')
                plt.savefig('plots/{}/wine-density-pH-em'.format(filepath, dataset['label']))	 
            else:           
                g = pairplot(new_df, x_vars=x_vars_bank, y_vars=y_vars_bank, hue='labels')
                g.fig.suptitle('Bank - EM Pairplot')
                plt.savefig('plots/{}/bank-density-pH-em'.format(filepath, dataset['label']))	

            plt.close()

        if filepath == 'part3':
            # Pairplot all
            new_df = pd.DataFrame(dataset['X'])
            new_df['labels'] = y_pred
            pairplot(new_df, hue='labels')
            plt.savefig('plots/{}/scatter-em-{}-all'.format(filepath, dataset['label']))
            plt.close()	

        print_and_write("{} | EM homogenuity: {}".format(dataset['label'], homog), filepath)
        print_and_write("{} | EM rand_score: {}".format(dataset['label'], rand), filepath)
        
    f.close()
