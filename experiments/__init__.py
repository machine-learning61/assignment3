from .part1 import part1
from .part2 import part2
from .part3 import part3
from .part4 import part4
from .part5 import part5