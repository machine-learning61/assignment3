import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time

from sklearn.decomposition import PCA, FastICA
from sklearn.neural_network import MLPClassifier
from sklearn.random_projection import SparseRandomProjection
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV, learning_curve

from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture

def print_and_write(string):
    print(string)
    f = open('part4.txt', "a")
    f.write(string + '\n')
    f.close()

def get_optimal_nn(X, y):
    parameters = {'hidden_layer_sizes': np.arange(10, 60, 10), 'learning_rate_init': [.1, .01, .001]}
    clf = GridSearchCV(MLPClassifier(max_iter=2000, batch_size=64), parameters, scoring='balanced_accuracy')
    clf.fit(X, y)

    params = clf.best_params_
    nn = MLPClassifier(hidden_layer_sizes=params['hidden_layer_sizes'], learning_rate_init=params['learning_rate_init'], max_iter=2000, batch_size=64)

    fit_time_sum = 0
    for k in range(5):
        start = time.time()
        nn.fit(X, y)
        end = time.time()
        fit_time_sum += (end - start)

    return clf.best_score_, fit_time_sum/5, nn.loss_curve_, params


def reduced_data_experiment(dataset):
    component_range = np.arange(1, len(dataset['X'].columns) + 1)
    algs = [
            {'instance': PCA, 'key': 'pca'},
            {'instance': FastICA, 'key': 'ica'},
            {'instance': SparseRandomProjection, 'key': 'rca'},
            {'instance': RandomForestClassifier, 'key': 'rf'},
        ]
    legend = ['PCA', 'ICA', 'RCA', 'RF', 'No dim. red (half data)', 'No dim. red (full data)']

    num_rows = len(dataset['X'].index)
    half_idx = round(num_rows / 2)
    reduced_X = dataset['X'].iloc[:half_idx]
    reduced_y = dataset['y_true'][:half_idx]
    # Base accuracy
    base_acc_full, base_fit_time_full, base_loss_curve, base_params = get_optimal_nn(dataset['X'], dataset['y_true'])
    base_acc, base_fit_time, base_loss_curve, base_params = get_optimal_nn(reduced_X, reduced_y)

    accuracy_vals = {
        'pca': [],
        'ica': [],
        'rca': [],
        'rf': [],
    }

    fit_time_vals = {
        'pca': [],
        'ica': [],
        'rca': [],
        'rf': [],
    }

    for i in component_range:
        for alg in algs:
            reduced_data = None
            if alg['key'] != 'rf':
                reduced_data = alg['instance'](n_components=i).fit_transform(dataset['X'])
            else:
                features = RandomForestClassifier().fit(dataset['X'], dataset['y_true']).feature_importances_
                reduced_features_arr = np.argpartition(features, -i)[-i:]
                reduced_data = dataset['X'].iloc[:, reduced_features_arr]
                
            acc, fit_time, _, _ = get_optimal_nn(reduced_data, dataset['y_true'])
            
            accuracy_vals[alg['key']].append(acc)
            fit_time_vals[alg['key']].append(fit_time)
    

    plot_title = "NN Accuracy with Dimension Reduction | Half training data"
    plt.figure(plot_title)
    plt.plot(component_range, accuracy_vals['pca'])
    plt.plot(component_range, accuracy_vals['ica'])
    plt.plot(component_range, accuracy_vals['rca'])
    plt.plot(component_range, accuracy_vals['rf'])
    plt.plot(component_range, np.full(len(component_range), base_acc), linestyle='dashed')
    plt.plot(component_range, np.full(len(component_range), base_acc_full), linestyle='dashed')
    plt.legend(legend)
    plt.title(plot_title)
    plt.xlabel('Number of Components')
    plt.ylabel('Balanced Accuracy')
    plt.savefig('plots/part4/nn-acc-{}-less-data'.format(dataset['label']))	
    plt.close(plot_title)

    plot_title = "NN Fit Times with Dimension Reduction | Half training data"
    plt.figure(plot_title)
    plt.plot(component_range, accuracy_vals['pca'])
    plt.plot(component_range, accuracy_vals['ica'])
    plt.plot(component_range, accuracy_vals['rca'])
    plt.plot(component_range, accuracy_vals['rf'])
    plt.plot(component_range, np.full(len(component_range), base_fit_time), linestyle='dashed')
    plt.plot(component_range, np.full(len(component_range), base_fit_time_full), linestyle='dashed')
    plt.title(plot_title)
    plt.legend(legend)
    plt.xlabel('Number of Components')
    plt.ylabel('Fit Time')
    plt.savefig('plots/part4/nn-fit-time-{}-less-data'.format(dataset['label']))	
    plt.close(plot_title)

def part4(data, filepath='part4'):
    print('\n----Part 4----')
    for dataset in data:
        reduced_data_experiment(dataset)
        return
        component_range = np.arange(1, len(dataset['X'].columns) + 1)
        algs = [
            {'instance': PCA, 'key': 'pca'},
            {'instance': FastICA, 'key': 'ica'},
            {'instance': SparseRandomProjection, 'key': 'rca'},
            {'instance': RandomForestClassifier, 'key': 'rf'},
        ]
        legend = ['PCA', 'ICA', 'RCA', 'RF', 'No dim. red']

        # Base accuracy
        base_acc, base_fit_time, base_loss_curve, base_params = get_optimal_nn(dataset['X'], dataset['y_true'])

        accuracy_vals = {
            'pca': [],
            'ica': [],
            'rca': [],
            'rf': [],
        }

        fit_time_vals = {
            'pca': [],
            'ica': [],
            'rca': [],
            'rf': [],
        }

        for i in component_range:
            for alg in algs:
                reduced_data = None
                if alg['key'] != 'rf':
                    reduced_data = alg['instance'](n_components=i).fit_transform(dataset['X'])
                else:
                    features = RandomForestClassifier().fit(dataset['X'], dataset['y_true']).feature_importances_
                    reduced_features_arr = np.argpartition(features, -i)[-i:]
                    reduced_data = dataset['X'].iloc[:, reduced_features_arr]
                    
                acc, fit_time, _, _ = get_optimal_nn(reduced_data, dataset['y_true'])
                
                accuracy_vals[alg['key']].append(acc)
                fit_time_vals[alg['key']].append(fit_time)
        

        plot_title = "NN Accuracy with Dimension Reduction"
        plt.figure(plot_title)
        plt.plot(component_range, accuracy_vals['pca'])
        plt.plot(component_range, accuracy_vals['ica'])
        plt.plot(component_range, accuracy_vals['rca'])
        plt.plot(component_range, accuracy_vals['rf'])
        plt.plot(component_range, np.full(len(component_range), base_acc), linestyle='dashed')
        plt.legend(legend)
        plt.title(plot_title)
        plt.xlabel('Number of Components')
        plt.ylabel('Balanced Accuracy')
        plt.savefig('plots/{}/nn-acc-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)

        plot_title = "NN Fit Times with Dimension Reduction"
        plt.figure(plot_title)
        plt.plot(component_range, accuracy_vals['pca'])
        plt.plot(component_range, accuracy_vals['ica'])
        plt.plot(component_range, accuracy_vals['rca'])
        plt.plot(component_range, accuracy_vals['rf'])
        plt.plot(component_range, np.full(len(component_range), base_fit_time), linestyle='dashed')
        plt.title(plot_title)
        plt.legend(legend)
        plt.xlabel('Number of Components')
        plt.ylabel('Fit Time')
        plt.savefig('plots/{}/nn-fit-time-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)

        # Generate Loss Curves
        NUM_COMPONENTS = {
            'pca': 2,
            'ica': 4,
            'rca': 9,
            'rf': 4
        }

        loss_curves = {
            'pca':  None,
            'ica':  None,
            'rca':  None,
            'rf': None,
        }

        learning_curves = {
            'pca':  None,
            'ica':  None,
            'rca':  None,
            'rf': None,
            'base': None
        }

        train_sizes, _, test_scores, _, _ = learning_curve(
                MLPClassifier(**base_params, max_iter=2000, batch_size=64),
                dataset['X'],
                dataset['y_true'], 
                scoring='balanced_accuracy',
                return_times=True
            )

        learning_curves['base'] = np.mean(test_scores, axis=1) 

        for alg in algs:
            print(alg['key'])
            reduced_data = None
            if alg['key'] != 'rf':
                reduced_data = PCA(n_components=NUM_COMPONENTS[alg['key']]).fit_transform(dataset['X'])
            else:
                features = RandomForestClassifier().fit(dataset['X'], dataset['y_true']).feature_importances_
                reduced_features_arr = np.argpartition(features, -NUM_COMPONENTS[alg['key']])[-NUM_COMPONENTS[alg['key']]:]
                reduced_data = dataset['X'].iloc[:, reduced_features_arr]
            
            _, _, loss_curve, params = get_optimal_nn(reduced_data, dataset['y_true'])
            loss_curves[alg['key']] = loss_curve

            _, _, test_scores_best, _, _ = learning_curve(
                MLPClassifier(**params, max_iter=2000, batch_size=64),
                reduced_data,
                dataset['y_true'], 
                scoring='balanced_accuracy',
                return_times=True
            )
            learning_curves[alg['key']] = np.mean(test_scores_best, axis=1)

        plot_title = "NN Loss Curves with Dimension Reduction"
        plt.figure(plot_title)
        plt.plot(loss_curves['pca'])
        plt.plot(loss_curves['ica'])
        plt.plot(loss_curves['rca'])
        plt.plot(loss_curves['rf'])
        plt.plot(base_loss_curve)
        plt.title(plot_title)
        plt.legend(legend)
        plt.xlabel('Number of Iters')
        plt.ylabel('Loss')
        plt.savefig('plots/{}/loss-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)

        plot_title = "NN Learning Curves with Dimension Reduction"
        plt.figure(plot_title)
        plt.plot(train_sizes, learning_curves['pca'])
        plt.plot(train_sizes, learning_curves['ica'])
        plt.plot(train_sizes, learning_curves['rca'])
        plt.plot(train_sizes, learning_curves['rf'])
        plt.plot(train_sizes, learning_curves['base'])
        plt.title(plot_title)
        plt.legend(legend)
        plt.xlabel('Training Size')
        plt.ylabel('Balanced Accuracy')
        plt.savefig('plots/{}/learning-curve-{}'.format(filepath, dataset['label']))	
        plt.close(plot_title)
