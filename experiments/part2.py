import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random

from sklearn.decomposition import PCA, FastICA
from sklearn.random_projection import SparseRandomProjection
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV

from scipy.stats import kurtosis
from sklearn.metrics import mean_squared_error



def get_optimal_knn(X, y):
        parameters = {'n_neighbors': np.arange(1, 10), 'weights': ['uniform', 'distance']}
        clf = GridSearchCV(KNeighborsClassifier(), parameters, scoring='balanced_accuracy')
        clf.fit(X, y)
        return clf.best_score_

def pca_experiment(dataset):
    # PCA
    component_range = np.arange(1, len(dataset['X'].columns) + 1)
    pca = PCA()
    pca.fit(dataset['X'])

    exp_var = pca.explained_variance_ratio_
    cum_exp_var = np.cumsum(exp_var)

    knn_vals = []
    # Generate knn_vals
    for i in component_range:
        components = PCA(n_components=i).fit_transform(dataset['X'])
        acc = get_optimal_knn(components, dataset['y_true'])
        knn_vals.append(acc)

    plot_title = "Dimension Reduction PCA - {}".format(dataset['label'])
    plt.figure(plot_title)

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    l1 = ax1.plot(component_range, exp_var, 'g-', label="Explained Variance")
    l2 = ax1.plot(component_range, cum_exp_var, 'r-', label="Cumulative Explained Variance")
    l3 = ax2.plot(component_range, knn_vals, 'b-', label="KNN Balanced Accuracy")
    ax1.legend(l1+l2+l3, ['Explained Variance', 'Cumulative Explained Variance', 'KNN Balanced Accuracy'])
    plt.title(plot_title)
    ax1.set_xlabel('Number of Components')
    ax1.set_ylabel('Explained Variance')
    ax2.set_ylabel('KNN Balanced Accuracy')
    plt.savefig('plots/part2/pca-{}'.format(dataset['label']))	
    plt.close(plot_title)

def ica_experiment(dataset):
    component_range = np.arange(1, len(dataset['X'].columns) + 1)
    kurtosis_vals = []
    knn_vals = []

    for num_cumponents in component_range:
        ica = FastICA(n_components=num_cumponents)
        components = ica.fit_transform(dataset['X'])
        kurtosis = pd.DataFrame(components).kurt(axis=0).abs().mean()
        kurtosis_vals.append(kurtosis)
    
        acc = get_optimal_knn(components, dataset['y_true'])
        knn_vals.append(acc)

    plot_title = "Dimension Reduction ICA - {}".format(dataset['label'])
    plt.figure(plot_title)
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    l1 = ax1.plot(component_range, kurtosis_vals, 'g-')
    l2 = ax2.plot(component_range, knn_vals, 'b-')
    ax1.legend(l1+l2, ['Kurtosis', 'KNN Balanced Accuracy'])
    plt.title(plot_title)
    ax1.set_xlabel('Number of Components')
    ax1.set_ylabel('Kurtosis')
    ax2.set_ylabel('KNN Balanced Accuracy')
    plt.savefig('plots/part2/ica-{}'.format(dataset['label']))	
    plt.close(plot_title)

def rca_experiment(dataset):
    component_range = np.arange(1, len(dataset['X'].columns) + 1)
    loss_vals = []
    knn_vals = []
    for i in component_range:
        loss_sum = 0
        for k in range(5):
            random.seed(k)
            rca = SparseRandomProjection(n_components=i, compute_inverse_components=True)

            components = rca.fit_transform(dataset['X'])
            
            X_new = rca.inverse_transform(components)
            loss = mean_squared_error(dataset['X'], X_new)
            loss_sum += loss
        loss_vals.append(loss_sum/5)

        acc = get_optimal_knn(components, dataset['y_true'])
        knn_vals.append(acc)
        
    plot_title = "Dimension Reduction RCA - {}".format(dataset['label'])
    plt.figure(plot_title)
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    l1 = ax1.plot(component_range, loss_vals, 'g-')
    l2 = ax2.plot(component_range, knn_vals, 'b-')
    ax1.legend(l1+l2, ['RCA Reconstruction Error', 'KNN Balanced Accuracy'])
    plt.title(plot_title)
    ax1.set_xlabel('Number of Components')
    ax1.set_ylabel('RCA Reconstruction Error')
    ax2.set_ylabel('KNN Balanced Accuracy')
    plt.savefig('plots/part2/rca-{}'.format(dataset['label']))	
    plt.close(plot_title)

def rf_experiment(dataset):
    component_range = np.arange(1, len(dataset['X'].columns) + 1)
    loss_vals = []
    knn_vals = []
    rf = RandomForestClassifier()
    rf.fit(dataset['X'], dataset['y_true'])
    features = rf.feature_importances_

    feature_importance = []
    argmax_features = []
    for i in component_range:
        feature_idx = np.argmax(features) # Grab most important feature
        argmax_features.append(feature_idx) # Add feature to arr
        feature_importance.append(features[feature_idx])
        features = np.delete(features, feature_idx) # Delete so we can properly argmax next iter
        
        X_new = dataset['X'].iloc[:, argmax_features]
        acc = get_optimal_knn(X_new, dataset['y_true'])
        knn_vals.append(acc)
    
    cum_feature_importance = np.cumsum(feature_importance)
    plot_title = "Dimension Reduction RF - {}".format(dataset['label'])
    plt.figure(plot_title)
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    l1 = ax1.plot(component_range, feature_importance, 'g-')
    l2 = ax2.plot(component_range, cum_feature_importance, 'r-')
    l3 = ax2.plot(component_range, knn_vals, 'b-')
    ax1.legend(l1+l2+l3, ['RF Feature Importance', 'RF Cumulative Feature Importance', 'KNN Balanced Accuracy'])
    plt.title(plot_title)
    ax1.set_xlabel('Number of Components')
    ax1.set_ylabel('RF Feature Importance')
    ax2.set_ylabel('RF Cumulative Feature Importance \n KNN Balanced Accuracy')
    plt.savefig('plots/part2/rf-{}'.format(dataset['label']))	
    plt.close(plot_title)


def part2(data):
    print('\n----Part 2----')
    for dataset in data:
        print('--{}--'.format(dataset['label']))
        pca_experiment(dataset)
        ica_experiment(dataset)
        rca_experiment(dataset)
        rf_experiment(dataset)