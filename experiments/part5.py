import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time

from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV, learning_curve

from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture

def print_and_write(string):
    print(string)
    f = open('part5.txt', "a")
    f.write(string + '\n')
    f.close()

def get_optimal_nn(X, y):
    parameters = {'hidden_layer_sizes': np.arange(10, 60, 10), 'learning_rate_init': [.1, .01, .001]}
    clf = GridSearchCV(MLPClassifier(max_iter=2000), parameters, scoring='balanced_accuracy')
    clf.fit(X, y)

    params = clf.best_params_
    nn = MLPClassifier(hidden_layer_sizes=params['hidden_layer_sizes'], learning_rate_init=params['learning_rate_init'], max_iter=2000)

    fit_time_sum = 0
    for k in range(5):
        start = time.time()
        nn.fit(X, y)
        end = time.time()
        fit_time_sum += (end - start)



    return clf.best_score_, fit_time_sum/5, nn.loss_curve_, params

def part5(data):
    print('\n----Part 5----')
    for dataset in data:
        # Base accuracy

        base_acc, base_fit_time, base_loss_curve, base_params = get_optimal_nn(dataset['X'], dataset['y_true'])
        train_sizes, _, base_test_scores, _, _  = learning_curve(
                MLPClassifier(**base_params, max_iter=1000, batch_size=64),
                dataset['X'],
                dataset['y_true'], 
                scoring='balanced_accuracy',
                return_times=True
            )

        algs = ['kmeans', 'em']
        accuracy_vals = {'kmeans': [], 'em': []}
        fit_time_vals = {'kmeans': [], 'em': []}

        cluster_range = np.arange(2, 21)
        for i in cluster_range:
            print(i)
            for alg in algs:
                fit_time_sum = 0
                labels = None
                if alg == 'kmeans':
                    kmeans = KMeans(n_clusters=i)
                    kmeans.fit(dataset['X']) # fit clusters
                    labels = kmeans.labels_ # Get labels
                else:
                    em = GaussianMixture(n_components=i)
                    labels = em.fit_predict(dataset['X'])

            
                new_df = pd.DataFrame(dataset['X']) # Create dummy dataframe
                new_df['cluster_assignments'] = labels # Put labels into DF
                acc, fit_time, _, _ = get_optimal_nn(new_df, dataset['y_true']) # Do NN stuff
                accuracy_vals[alg].append(acc)
                fit_time_vals[alg].append(fit_time)

        legend = ['Kmeans', 'EM', 'No Clustering']

        plot_title = "NN Accuracy with Clustering"
        plt.figure(plot_title)
        plt.plot(cluster_range, accuracy_vals['kmeans'])
        plt.plot(cluster_range, accuracy_vals['em'])
        plt.plot(cluster_range, np.full(len(cluster_range), base_acc), linestyle='dashed')
        plt.legend(legend)
        plt.title(plot_title)
        plt.xlabel('Number of Clusters')
        plt.ylabel('Balanced Accuracy')
        plt.savefig('plots/part5/nn-acc-{}'.format(dataset['label']))	
        plt.close(plot_title)

        plot_title = "NN Fit Times with Clustering"
        plt.figure(plot_title)
        plt.plot(cluster_range, fit_time_vals['kmeans'])
        plt.plot(cluster_range, fit_time_vals['em'])
        plt.plot(cluster_range, np.full(len(cluster_range), base_fit_time), linestyle='dashed')
        plt.title(plot_title)
        plt.legend(legend)
        plt.xlabel('Number of Clusters')
        plt.ylabel('Fit Time')
        plt.savefig('plots/part5/nn-fit-time-{}'.format(dataset['label']))	
        plt.close(plot_title)

        # Learning and Loss Curves
        KMEANS_NUM_CLUSTERS = {
            'bank': 3, 
            'wine': 2,
            
        }
        EM_NUM_CLUSTERS = {
            'bank': 9,
            'wine': 5,
        }

        # KMeans
        kmeans = KMeans(n_clusters=KMEANS_NUM_CLUSTERS[dataset['label']]).fit(dataset['X'])
        labels = kmeans.labels_

        new_df = pd.DataFrame(dataset['X']) # Create dummy dataframe
        new_df['cluster_assignments'] = labels # Put labels into DF
        acc, fit_time, kmeans_loss_curve, kmeans_params = get_optimal_nn(new_df, dataset['y_true']) # Do NN stuff
        train_sizes, _, test_scores_kmeans, _, _ = learning_curve(
                MLPClassifier(**kmeans_params, max_iter=1000, batch_size=64),
                new_df,
                dataset['y_true'], 
                scoring='balanced_accuracy',
                return_times=True
        )

        # EM
        em = GaussianMixture(n_components=EM_NUM_CLUSTERS[dataset['label']])
        labels = em.fit_predict(dataset['X'])

        new_df = pd.DataFrame(dataset['X']) # Create dummy dataframe
        new_df['cluster_assignments'] = labels # Put labels into DF
        acc, fit_time, em_loss_curve, em_params = get_optimal_nn(new_df, dataset['y_true']) # Do NN stuff
        train_sizes, _, test_scores_em, _, _ = learning_curve(
                MLPClassifier(**em_params, max_iter=1000, batch_size=64),
                new_df,
                dataset['y_true'], 
                scoring='balanced_accuracy',
                return_times=True
        )

        # Loss Curve
        plot_title = "NN Loss Curves with Clustering"
        plt.figure(plot_title)
        plt.plot(kmeans_loss_curve)
        plt.plot(em_loss_curve)
        plt.plot(base_loss_curve)
        plt.title(plot_title)
        plt.legend(legend)
        plt.xlabel('Number of Iters')
        plt.ylabel('Loss')
        plt.savefig('plots/part5/loss-{}'.format(dataset['label']))	
        plt.close(plot_title)


        # Learning Curve
        plot_title = "NN Learning Curves with Clustering"
        plt.figure(plot_title)
        plt.plot(train_sizes, np.mean(test_scores_kmeans, axis=1))
        plt.plot(train_sizes, np.mean(test_scores_em, axis=1))
        plt.plot(train_sizes, np.mean(base_test_scores, axis=1))
        plt.title(plot_title)
        plt.legend(legend)
        plt.xlabel('Training Size')
        plt.ylabel('Balanced Accuracy')
        plt.savefig('plots/part5/learning-curve-{}'.format(dataset['label']))	
        plt.close(plot_title)

