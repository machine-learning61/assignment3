import argparse
import time
import csv
from sklearn.preprocessing import LabelEncoder

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from experiments import part1, part2, part3, part4, part5


'''
Code breakdown

1. Read in data
2. Optionally reduce data
3. Optionally cluster data
4. NN

'''

def main():
    # Handle Args
    parser = argparse.ArgumentParser(description='CS7641 Assignment-3')
    parser.add_argument('--clustering', action='store_true')


    parser.add_argument('--both', action='store_true')
    parser.add_argument('--bank', action='store_true')
    parser.add_argument('--wine', action='store_true')
    args = parser.parse_args()

    print(args)

    data = []
    df_wine = pd.read_csv('data/winequality-red.csv', sep=";")
    bins = (2, 4.5, 6.5, 9)
    group_names = ['bad', 'average', 'good']
    # Change Quality Labels from values to classes
    df_wine['quality'] = pd.cut(df_wine['quality'], bins = bins, labels = group_names)
    y_wine = df_wine['quality'].to_numpy()
    df_wine = df_wine.drop('quality', axis=1)

    data.append({'label': 'wine', 'X': df_wine, 'y_true': y_wine})

    df_bank = pd.read_csv('data/bank-additional.csv', sep=';')
    for col in df_bank:
        if isinstance(df_bank[col].iloc[0], str):
            le = LabelEncoder()
            le.fit(df_bank[col])
            df_bank[col] = le.transform(df_bank[col])
    
    y_bank = df_bank['y'].to_numpy()
    df_bank = df_bank.drop('y', axis=1)
    data.append({'label': 'bank', 'X': df_bank, 'y_true': y_bank})

    part1(data)
    part2(data)
    part3(data)
    # Just pass in wine
    part4([data[0]])
    part5([data[0]])
   
if __name__ == '__main__':
    main()
