Part 1 Results
len: 1179 {'average': 933, 'bad': 52, 'good': 194}
len: 420 {'average': 386, 'bad': 11, 'good': 23}
wine | Kmeans homogenuity: 0.02342276546108522
wine | Kmeans rand_score: 0.5161595834693304
len: 503 {'average': 362, 'bad': 30, 'good': 111}
len: 185 {'average': 137, 'bad': 13, 'good': 35}
len: 205 {'average': 195, 'bad': 6, 'good': 4}
len: 704 {'average': 625, 'bad': 14, 'good': 65}
len: 2 {'good': 2}
wine | EM homogenuity: 0.06165646015493206
wine | EM rand_score: 0.4440408233869573
len: 3471 {0: 3309, 1: 162}
len: 490 {0: 299, 1: 191}
len: 158 {0: 60, 1: 98}
bank | Kmeans homogenuity: 0.2359599464130388
bank | Kmeans rand_score: 0.7794541482682332
len: 139 {0: 103, 1: 36}
len: 22 {0: 3, 1: 19}
len: 383 {0: 365, 1: 18}
len: 16 {0: 8, 1: 8}
len: 712 {0: 644, 1: 68}
len: 11 {0: 4, 1: 7}
len: 321 {0: 216, 1: 105}
len: 2378 {0: 2268, 1: 110}
len: 137 {0: 57, 1: 80}
bank | EM homogenuity: 0.19534630208116144
bank | EM rand_score: 0.49706102602505053
