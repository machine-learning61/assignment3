Link to code repo: https://gitlab.com/machine-learning61/assignment3

Links to Datasets:
    Wine Quality: http://archive.ics.uci.edu/ml/datasets/Wine+Quality
    Bank Marketing: http://archive.ics.uci.edu/ml/datasets/Bank+Marketing#

3rd Party Libraries Used:
- Sklearn, For all metrics, clustering algorithms, reduction algorithms: https://scikit-learn.org/
- Seaborn, for pair plots: https://seaborn.pydata.org/index.html

Python version used: Python 3.9.13

How to run Code:
    1. Create virtual environment: python3 -m venv env
    2. Activate virtual environment: source env/bin/activate
    3. Install dependencies: pip install -r requirements.txt
    4. Run code: python main.py
            